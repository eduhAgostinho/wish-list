exports.seed = function(knex) {
    return knex('categories').del()
    .then(() => knex('categories').insert([
      { id: 1, description: 'clothes' },
      { id: 2, description: 'electronics' },
    ]));
};
  