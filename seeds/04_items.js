exports.seed = function(knex) {
    return knex('items').del()
    .then(() => knex('items').insert([
        { 
            wish_id: 1,
            link: 'https://www.apple.com/br/iphone-13/',
            price: 7599.00,
            description: 'Iphone 13'
        },
        { 
            wish_id: 1,
            link: 'https://www.apple.com/br/airpods/',
            price: 2399.00,
            description: 'AirPods (3ª geração)'
        },
    ]));
};
  