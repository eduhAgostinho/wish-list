exports.seed = function(knex) {
  return knex('users').del()
  .then(() => knex('users').insert([
    { id: 1, username: 'johnDoe', password_hash: '$2b$10$HCAS/fkmyJSBNYFflvt8XO6w4wVBLxQccBB9Af7B.rEgNn1oCznH2', current_token: '11131' },
    { id: 2, username: 'janeDoe', password_hash: '$2b$10$HCAS/fkmyJSBNYFflvt8XO6w4wVBLxQccBB9Af7B.rEgNn1oCznH2', current_token: '11131' },
  ]));
};
