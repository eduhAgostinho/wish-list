import { IsInt, IsNotEmpty, IsOptional, IsString } from "class-validator";

export class UpdateUserDto {
    @IsNotEmpty()
    @IsInt()
    id: number;
    
    @IsNotEmpty()
    @IsString()
    username: string;

    @IsOptional()
    @IsString()
    password?: string;
   
    @IsOptional()
    @IsString()
    current_token?: string;
}
