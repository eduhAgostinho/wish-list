import { Body, Controller, Delete, Get, Param, ParseIntPipe, Post, Put, Res, UseGuards } from '@nestjs/common';
import { UsersService } from './users.service';
import { Response } from 'express';
import { CreateUserDto } from './dto/createUserDto';
import { UpdateUserDto } from './dto/updateUserDto';
import { JwtAuthGuard } from 'src/auth/jwt/jwt-auth.guard';

@Controller('users')
export class UsersController {
    
    constructor(private readonly userService: UsersService) {}

    @Get()
    @UseGuards(JwtAuthGuard)
    async find() {
        return await this.userService.getUser();
    }

    @Get(':id')
    @UseGuards(JwtAuthGuard)
    async findOne(@Res({ passthrough: true }) res: Response, @Param('id', ParseIntPipe) id: number) {
        const user = await this.userService.getUser(id);
        res.status(user ? 200 : 404);
        return user;
    }

    @Post()
    @UseGuards(JwtAuthGuard)
    create(@Body() userDto: CreateUserDto) {
        return this.userService.createUser(userDto);
    }

    @Put(':id')
    @UseGuards(JwtAuthGuard)
    update(@Param('id', ParseIntPipe) id: number, @Body() userDto: UpdateUserDto) {
        return this.userService.updateUser(id, userDto);
    }

    @Delete(':id')
    @UseGuards(JwtAuthGuard)
    delete(@Param('id', ParseIntPipe) id: number) {
        return this.userService.deleteUser(id);
    }
} 
              
