export interface UserCreateModel {
    username: string;
    password_hash: string;
    current_token?: string;
}
