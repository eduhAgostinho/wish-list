import { UserCreateModel } from './userCreateModel';

export interface UserUpdateModel extends UserCreateModel {
    id: number;
}
