import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { UserDAO } from './DAO/userDAO';
import { CreateUserDto } from './dto/createUserDto';
import { UpdateUserDto } from './dto/updateUserDto';
import * as bcrypt from 'bcrypt';
@Injectable()
export class UsersService {

    constructor(private userDAO: UserDAO) {}

    getUser(id?: number) {
        return id ? this.userDAO.findBy('id', id) : this.userDAO.get();
    }

    async createUser(userDto: CreateUserDto) {
        const user = await this.userDAO.findBy('username', userDto.username);
        if (user) {
            throw new HttpException('Username duplicate', HttpStatus.BAD_REQUEST);
        }
        const hash = await bcrypt.hash(userDto.password, 10);

        const { password, ...newUSer } = userDto; 
        return this.userDAO.create({ ...newUSer, password_hash: hash });
    }

    async updateUser(id: number, userDto: UpdateUserDto) {
        const user = await this.findOneByUsername(userDto.username);
        if (user && user.id !== id) {
            throw new HttpException('Username duplicate', HttpStatus.BAD_REQUEST);
        } else if (id !== userDto.id) {
            throw new HttpException('Id duplicate', HttpStatus.BAD_REQUEST);
        }
        const { password, ...newUSer } = userDto; 
        return this.userDAO.update(id, { ...newUSer, password_hash: password });
    }

    deleteUser(id: number) {
        return this.userDAO.delete(id);
    }

    findOneByUsername(username: string) {
        return this.userDAO.findBy('username', username);
    }
}

