import { Module } from '@nestjs/common';
import { UserDAO } from './DAO/userDAO';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';

@Module({
  controllers: [UsersController],
  providers: [UsersService, UserDAO],
  exports: [UsersService]
})
export class UsersModule {}
