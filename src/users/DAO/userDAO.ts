import { Injectable } from "@nestjs/common";
import { Knex } from "knex";
import { InjectModel } from "nest-knexjs";
import { UserCreateModel } from "../models/userCreateModel";
import { UserUpdateModel } from "../models/userUpdateMode";

@Injectable()
export class UserDAO {
    constructor(@InjectModel() private readonly knex: Knex) {}

    private get users() {
        return this.knex.table('users');
    }

    get() {
        return this.users.select('*');
    }

    findBy(field: 'id' | 'username', value: number | string): Promise<UserUpdateModel | null> {
        return this.get().where({[field]: value}).first();
    }

    create(user: UserCreateModel) {
        return this.users.insert(user);
    }

    update(id: number, user: UserUpdateModel) {
        return this.users.where({id}).update(user);
    }

    delete(id: number) {
        return this.users.where({id}).del();
    }
}
