import { Injectable } from "@nestjs/common";
import { Knex } from "knex";
import { InjectModel } from "nest-knexjs";
import { Item } from "../models/item";
import { Wish, WishRespose } from "../models/wish";

@Injectable()
export class WishesDAO {
    
    constructor(@InjectModel() private readonly knex: Knex) {}

    private get wishes() {
        return this.knex.table('wishes as w');
    }

    private baseQuery()  {
        return this.wishes
        .select('w.*', 'c.description as category_description')
        .leftJoin('categories as c', 'w.category_id', 'c.id');
    }

    private formatCategory = (category: Wish) => {
        if (category) {
            const { category_description, category_id, ...item } = category;
            return { ...item, category: { id: category_id, description: category_description } };
        }
        return null;
    }

    get(): Promise<WishRespose[]> {
        return (this.baseQuery() as Promise<Wish[]>).then(result => {
            return result.map(this.formatCategory);
        });
    }

    findOne(id: number): Promise<WishRespose> {
        return this.baseQuery()
        .where('w.id', id)
        .first()
        .then(this.formatCategory);
    }

    getItemsByWish(idWish: number): Promise<Item[]> {
        return this.wishes.select('i.*')
        .rightJoin('items as i', 'w.id', 'i.wish_id')
        .where('w.id', idWish);
    }

    getItem(idWish: number, idItem: number): Promise<Item> {
        return this.wishes.select('i.*')
        .rightJoin('items as i', 'w.id', 'i.wish_id')
        .where('w.id', idWish)
        .andWhere('i.id', idItem)
        .first();
    }
}
