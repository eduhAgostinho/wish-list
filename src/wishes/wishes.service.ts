import { Injectable } from '@nestjs/common';
import { WishesDAO } from './DAO/wishesDAO';

@Injectable()
export class WishesService {

    constructor(private wishesDAO: WishesDAO) {}

    get() {
        return this.wishesDAO.get();
    }

    getWish(id: number) {
        return this.wishesDAO.findOne(id);
    }

    getItemsByWish(idWish: number) {
        return this.wishesDAO.getItemsByWish(idWish);
    }

    getItem(idWish: number, idItem: number) {
        return this.wishesDAO.getItem(idWish, idItem)
    }
}

