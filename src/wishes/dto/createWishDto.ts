import { IsNotEmpty, IsOptional, IsString, IsInt, MaxLength, IsNumber, IsDateString } from "class-validator";

export class CreateWishDto {
    @IsNotEmpty()
    @IsInt()
    user_id: number;
    
    @IsNotEmpty()
    @IsInt()
    category_id: number;
    
    @IsNotEmpty()
    @IsString()
    @MaxLength(255)
    title: string;

    @IsNotEmpty()
    @IsString()
    @MaxLength(1000)
    description: string;

    @IsOptional()
    @IsNumber()
    budget: number;
  
    @IsOptional()
    @IsDateString()
    due_date: string;
    
    @IsNotEmpty()
    @IsDateString()
    created_at: string;
}
