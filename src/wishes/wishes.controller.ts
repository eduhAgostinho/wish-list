import { Controller, Get, Param, ParseIntPipe, Res } from '@nestjs/common';
import { Response } from 'express';
import { WishesService } from './wishes.service';

@Controller('wishes')
export class WishesController {

    constructor(private wishesService: WishesService) {}

    @Get()
    get() {
        return this.wishesService.get();
    }

    @Get(':id')
    async findOne(@Res({ passthrough: true }) res: Response, @Param('id', ParseIntPipe) id: number) {
        const wishes = await this.wishesService.getWish(id);
        res.status(wishes ? 200 : 404);
        return wishes;
    }

    @Get(':id/items')
    getItems(@Res({ passthrough: true }) res: Response, @Param('id', ParseIntPipe) id: number) {
        return this.wishesService.getItemsByWish(id);
    }
    
    @Get(':idWish/items/:idItem')
    getOneItem(@Res({ passthrough: true }) res: Response, @Param('idWish', ParseIntPipe) idWish: number, @Param('idItem', ParseIntPipe) idItem: number) {
        return this.wishesService.getItem(idWish, idItem);
    }
}

