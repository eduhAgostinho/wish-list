import { Module } from '@nestjs/common';
import { WishesService } from './wishes.service';
import { WishesController } from './wishes.controller';
import { WishesDAO } from './DAO/wishesDAO';

@Module({
  providers: [WishesService, WishesDAO],
  controllers: [WishesController]
})
export class WishesModule {}
