interface WishBase {
    id: number;
    user_id: number;
    title: string;
    description: string;
    budget: number;
    due_date: string;
    created_at: string;
}

export interface Wish extends WishBase {
    category_id: number;
    category_description?: string;
}

export interface WishRespose extends WishBase {
    category: {
        id: number;
        description: string;
    }
}
