export interface Item {
    id: number,
    wish_id: number,
    link: string,
    price: number,
    description: string,
    rate: number,
    created_at: string
}
