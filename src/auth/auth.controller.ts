import { Controller, Post, Body } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthLoginDto } from './dto/authLoginDto';

@Controller('auth')
export class AuthController {

  constructor(private authService: AuthService) {}

  @Post()
  async login(@Body() loginDto: AuthLoginDto) {
    return this.authService.login(loginDto);
  }
}
