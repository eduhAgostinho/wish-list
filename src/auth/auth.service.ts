import { Injectable, UnauthorizedException } from '@nestjs/common';
import { UsersService } from 'src/users/users.service';
import { JwtService } from '@nestjs/jwt';
import { AuthLoginDto } from './dto/authLoginDto';
import * as bcrypt from 'bcrypt';
@Injectable()
export class AuthService {
    
    constructor(
      private usersService: UsersService,
      private jwtService: JwtService) {}

    async validateUser({ username, password }) {
      const user = await this.usersService.findOneByUsername(username);

      if (user && (await bcrypt.compare(password, user.password_hash))) {
        const { password_hash, ...result } = user;
        return result;
      }
      return null;
    }

    async login(userDto: AuthLoginDto) {
      const user = await this.validateUser(userDto);
      
      if (!user) {
        throw new UnauthorizedException();
      }

      return this.createToken(user);
    }

    createToken(user) {
      return { access_token: this.jwtService.sign({ user }) }
    }
}
