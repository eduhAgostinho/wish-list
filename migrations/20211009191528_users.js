
exports.up = function(knex) {
    return knex.schema
    .createTable('users', function (table) {
       table.increments('id');
       table.string('username', 255).unique().notNullable();
       table.string('password_hash', 255).notNullable();
       table.string('current_token', 1000);
    });
};

exports.down = function(knex) {
    return knex.schema.dropTable("users");
};
