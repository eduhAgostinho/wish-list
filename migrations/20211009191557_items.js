
exports.up = function(knex) {
    return knex.schema
    .createTable('items', function (table) {
        table.increments('id');
        table.integer('wish_id')
        .unsigned()
        .references('id')
        .inTable('wishes');
        table.string('link', 255).notNullable();
        table.float('price').notNullable();
        table.string('description', 1000).notNullable();
        table.integer('rate');
        table.timestamp('created_at').defaultTo(knex.fn.now());
    });
};

exports.down = function(knex) {
    return knex.schema.dropTable("items");
};
