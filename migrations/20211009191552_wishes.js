
exports.up = function(knex) {
    return knex.schema
    .createTable('wishes', function (table) {
       table.increments('id');
       table.integer('user_id')
        .unsigned()
        .references('id')
        .inTable('users');
       table.integer('category_id')
        .unsigned()
        .references('id')
        .inTable('categories');
        table.string('title', 255).notNullable();
        table.string('description', 1000).notNullable();
        table.float('budget');
        table.timestamp('due_date');
        table.timestamp('created_at').defaultTo(knex.fn.now());
    });
};

exports.down = function(knex) {
    return knex.schema.dropTable("wishes");
};
